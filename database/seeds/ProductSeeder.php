<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Product;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = $this->getData();

        foreach ($data as $product) {
        	Product::firstOrCreate($product);
        }

        if (env('DB_CONNECTION') == 'pgsql') {
        	DB::statement("SELECT setval(pg_get_serial_sequence('products', 'id'), coalesce(max(id)+1, 1), false) FROM products");
        }
    }


    private function getData(): array
    {
        return [
        	[
                'id' => 1,
                'name' => 'Playstation 5',
                'price' => 400.00,
                'current_quantity' => 5
            ],
            [
                'id' => 2,
                'name' => 'Playstation 4',
                'price' => 200.00,
                'current_quantity' => 20
            ],           
            [
                'id' => 3,
                'name' => 'Nintendo Switch',
                'price' => 2000.00,
                'current_quantity' => 99
            ]
        ];
    }
}
