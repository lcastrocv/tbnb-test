<?php

namespace App\Observers;

use App\Models\Product;

class ProductObserver
{
    /**
     * Handle the Product "creating" event.
     *
     * @param  \App\Product  $product
     * @return void
     */
    public function created(Product $product)
    {
        $product->history()->create([
            'old_quantity' => null,
            'new_quantity' => $product->current_quantity
        ]);
    }

    /**
     * Handle the Product "created" event.
     *
     * @param  \App\Product  $product
     * @return void
     */
    public function creating(Product $product)
    {
        //
    }

    /**
     * Handle the Product "saved" event.
     *
     * @param  \App\Product  $product
     * @return void
     */
    public function updated(Product $product)
    {
        $oldQuantity = $product->getOriginal('current_quantity');
        $newQuantity = $product->current_quantity;

        if ($oldQuantity != $newQuantity) {
            $product->history()->create([
                'old_quantity' => $oldQuantity,
                'new_quantity' => $newQuantity
            ]);
        }
    }

    /**
     * Handle the Product "deleted" event.
     *
     * @param  \App\Product  $product
     * @return void
     */
    public function deleted(Product $product)
    {
        //
    }

    /**
     * Handle the Product "restored" event.
     *
     * @param  \App\Product  $product
     * @return void
     */
    public function restored(Product $product)
    {
        //
    }

    /**
     * Handle the Product "force deleted" event.
     *
     * @param  \App\Product  $product
     * @return void
     */
    public function forceDeleted(Product $product)
    {
        //
    }
}
