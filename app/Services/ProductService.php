<?php

namespace App\Services;

use App\Models\Product;
use App\Repositories\BaseRepository;
use App\Repositories\Contracts\IProductRepository;
use Carbon\Carbon;

class ProductService extends BaseRepository implements IProductRepository
{
    public function __construct(Product $product)
    {
        $this->model = $product;
    }
    public function bulkInsert($data)
    {


        //Observables dont't track batch inserts using $model->insert(). 
        //To generate the initial history entry, looping through products
        //was necessary
        foreach ($data['products'] as $product) {
            $productObject = $this->model->create($product);   
        }
    }   
    public function bulkUpdate($data)
    {
    	$productsById = [];
    	foreach ($data['products'] as $product) {
    		$productsById[$product['id']] = [
    			'name' => $product['name'],
                'price' => $product['price'],
    			'current_quantity' => $product['current_quantity']
    		];
    	}

    	$products = $this->model->whereIn('id', array_keys($productsById))->get();

    	foreach ($products as $product) {
    		$dataToUpdate = $productsById[$product->id];
    		$product->update($dataToUpdate);
    	}
    }

    public function getProductQuantityHistory($product, $request)
    {
        $product = $product->history();

        if ($request->start_date && $request->end_date) {
            $start_date = Carbon::parse($request->start_date)->startOfDay();
            $end_date = Carbon::parse($request->end_date)->endOfDay();
            $product->whereBetween('created_at', [$start_date, $end_date]);
        }

        return $product->get()->toArray();
    }
}
