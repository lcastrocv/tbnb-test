<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Repositories\Contracts\IProductRepository;
use App\Http\Requests\ProductRequest;

class ProductController extends BaseController
{
	public function __construct(IProductRepository $productRepository)
	{
		$this->productRepository = $productRepository;
	}
	public function paginated(Request $request)
	{
		return $this->productRepository->paginate($request->itemsPerPage);
	}
	public function store(ProductRequest $request)
	{
		$this->productRepository->bulkInsert($request->validated());
        return response()->json(null, JsonResponse::HTTP_NO_CONTENT);

	}	
	public function update(ProductRequest $request)
	{
		$this->productRepository->bulkUpdate($request->validated());
        return response()->json(null, JsonResponse::HTTP_NO_CONTENT);

	}
	public function destroy(Product $product)
	{
		$this->productRepository->delete($product->id);
        return response()->json(null, JsonResponse::HTTP_NO_CONTENT);
	}
	public function getProductQuantityHistory(Product $product, Request $request)
	{
		return $this->productRepository->getProductQuantityHistory($product, $request);
	}
}
