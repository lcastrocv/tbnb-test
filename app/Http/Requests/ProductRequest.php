<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->getMethod() == 'POST') {
            return [
                "products.*.name" => "required|string|max:128",
                "products.*.current_quantity" => "required|integer",
                "products.*.price" => "required|numeric",
            ];
        } else if ($this->getMethod() == 'PUT') {
            return [
                "products.*.id" => "required",
                "products.*.name" => "required|string|max:128",
                "products.*.current_quantity" => "required|integer",
                "products.*.price" => "required|numeric",
            ];
        }
    }
}
