<?php

namespace App\Repositories;

use App\Repositories\Contracts\IBaseRepository;
use Illuminate\Database\Eloquent\Model;

class  BaseRepository implements IBaseRepository
{
    protected $model;
    /**
     * BaseRepository constructor.
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }
    public function create(array $attributes)
    {

        return $this->model->create($attributes);
    }
    /**
     * @param array $data
     * @return bool
     */
    public function update($id, array $data)
    {
        return  $this->model->findOrFail($id)->update($data);
    }
    /**
     * @param array $columns
     * @param string $orderBy
     * @param string $sortBy
     * @return mixed
     */
    public function paginate()
    {
        $query = $this->model->query();
        if (request('order') && request('direction')) {
            $query->orderBy(request('order'), request('direction'));
        }
        if (request('search')) {
            $query->where('name', 'like', '%' . request('search') . '%');
        }
        return $query->paginate(request('limit') ? request('limit') : 10);
    }
    /**
     * @param array $columns
     * @param string $orderBy
     * @param string $sortBy
     * @return mixed
     */
    public function all($columns = ['*'])
    {
        return $this->model->get($columns);
    }
    /**
     * @param string $id
     * @return mixed
     */
    public function find($id)
    {
        return $this->model->find($id);
    }
    /**
     * @param  $id
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function findOneOrFail($id)
    {
        return $this->model->findOrFail($id);
    }
    /**
     * @param array $data
     * @return Collection
     */
    public function findBy(array $data)
    {
        return $this->model->where($data)->get();
    }
    /**
     * @param array $data
     * @return mixed
     */
    public function findOneBy(array $data)
    {
        return $this->model->where($data)->first();
    }
    /**
     * @param array $data
     * @return mixed
     */
    public function findOneByOrFail(array $data)
    {
        return $this->model->where($data)->firstOrFail();
    }
    /**
     * @return bool
     * @throws \Exception
     */
    public function delete($id)
    {
        return $this->model->findOrFail($id)->delete();
    }
}
