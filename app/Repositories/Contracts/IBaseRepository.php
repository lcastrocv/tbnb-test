<?php

namespace App\Repositories\Contracts;

interface IBaseRepository
{

    /**
     * @param array $attributes
     * @return mixed
     */
    public function create(array $attributes);
    /**
     * @param array $attributes
     * @return bool
     */
    public function update($id, array $attributes);
    /**
     * @param array $columns
     * @param string $orderBy
     * @param string $sortBy
     * @return mixed
     */

    /**
     * @param array $columns
     * @param string $orderBy
     * @param string $sortBy
     * @return mixed
     */
    public function paginate();

    public function all($columns = array('*'));
    /**
     * @param $id
     * @return mixed
     */
    public function find($id);
    /**
     * @param $id
     * @return mixed
     */
    public function findOneOrFail($id);
    /**
     * @param array $data
     * @return mixed
     */
    public function findBy(array $data);
    /**
     * @param array $data
     * @return mixed
     */
    public function findOneBy(array $data);
    /**
     * @param array $data
     * @return mixed
     */
    public function findOneByOrFail(array $data);
    /**
     * @return bool
     */
    public function delete($id);
    /**
     * @param array $data
     * @param int $perPage
     */
}
