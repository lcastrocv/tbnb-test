<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
	use SoftDeletes;

    protected $fillable = ['name', 'current_quantity', 'price', 'created_at', 'updated_at'];

    public function history()
    {
    	return $this->hasMany(ProductHistory::class);
    }
}
