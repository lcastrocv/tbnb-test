<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function () {
	Route::resource('products', 'Api\v1\ProductController');
	Route::get('/products', 'Api\v1\ProductController@paginated');
	Route::put('/products/batch-update', 'Api\v1\ProductController@update');
	Route::get('/products/{product}/history', 'Api\v1\ProductController@getProductQuantityHistory');
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
