<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Faker\Factory;
use App\Models\Product;
use Carbon\Carbon;

use Tests\TestCase;

class ProductTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_index_view()
    {
        $response = $this->get('/api/v1/products');

        $response->assertStatus(200);
    }

    public function test_bulk_create_products()
    {
        $products = [];

        $faker = Factory::create();

        for ($i=0; $i < 3; $i++) { 
            $products['products'][] = [
                'name'  => $faker->firstName,
                'current_quantity' => $faker->randomNumber,
                'price' => $faker->randomFloat(2, 10000, 0.1)
            ];
        }

        $response = $this->post('/api/v1/products', $products);

        foreach ($products['products'] as $product) {
            $this->assertDatabaseHas('products', $product);
        }

        $response->assertStatus(204);
    }

    public function test_bulk_update_products()
    {
        $products = [];
        $createdProducts = [];
        $faker = Factory::create();

        for ($i=0; $i < 3; $i++) { 
            
            $product = $this->createProduct();
            $createdProducts[] = $product;

            $products['products'][] = [
                'id'    => $product->id,
                'name'  => $faker->firstName,
                'current_quantity' => $faker->randomNumber,
                'price' => $faker->randomFloat(2, 10000, 0.1)
            ];
        }


        $response = $this->put('/api/v1/products/batch-update', $products);
        
        $this->assertDatabaseCount('products', count($createdProducts));

        foreach ($products['products'] as $product) {
            $this->assertDatabaseHas('products', $product);
        }

        $response->assertStatus(204);

    }

    public function test_view_product_history()
    {
        $faker = Factory::create();

        $product = $this->createProduct();

        $response = $this->get('/api/v1/products/' . $product->id . '/history');

        $response->assertStatus(200);
        $response->assertJsonStructure([
            '*' => [
                'id',
                'product_id',
                'old_quantity',
                'new_quantity',
                'created_at',
                'updated_at'
            ]
        ]);
    }

    public function test_update_product_history()
    {
        $faker = Factory::create();

        $product = $this->createProduct();

        //structuring array to pass to update function
        $productsToUpdateArray = [
            $product->toArray()
        ];

        $lastHistory = $product->history()
            ->first()
            ->toArray();

        //updating array with new quantity information to trigger history observable
        $productsToUpdateArray[0]['current_quantity'] = $faker->randomDigit;

        $response = $this->put('/api/v1/products/batch-update', ['products' => $productsToUpdateArray ]);


        $response->assertStatus(204);

        $this->assertDatabaseHas('product_history', $lastHistory);

        $this->assertDatabaseHas('product_history',
            [
                'product_id'   => $product->id,
                'old_quantity' => $lastHistory['new_quantity'],
                'new_quantity' => $productsArray[0]['current_quantity']
            ]
        );

        
        $response = $this->get('/api/v1/products/' . $product->id . '/history');

        $response->assertStatus(200);
        $response->assertJsonStructure([
            '*' => [
                'id',
                'product_id',
                'old_quantity',
                'new_quantity',
                'created_at',
                'updated_at'
            ]
        ]);
    }

    public function test_assert_product_delete()
    {
        $productToBeDeleted = $this->createProduct();

        $response = $this->delete('/api/v1/products/' . $productToBeDeleted['id']);
        
        $response->assertStatus(204);

        $this->assertSoftDeleted('products', $productToBeDeleted->fresh()->toArray());
    }

    public function createProduct()
    {
        return factory(Product::class)->create();
    }
}
