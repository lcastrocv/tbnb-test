# TurnoverBNB Application - Challenge Project

A simple product management system made for the TurnoverBNB's Application Challenge ([GitHub - TidyDaily/tbnb-test](https://github.com/TidyDaily/tbnb-test)).

## Technologies
- PHP 7.2
 - Laravel 7.2
 - VueJS 2.5 with Vuetify Framework
## Extra Packages and Plugins
- MomentJS
- Vue Router
- Vuetify Mask
## Application Features

The application is built following the **Repository pattern**, which adds an additional logic service layer between the Controller and the Model, removing all logic and data manipulation from both layers.

All logic are encapsulated on the **Services** files ([lcastrocv / tbnb-test / app / Services — Bitbucket](https://bitbucket.org/lcastrocv/tbnb-test/src/master/app/Services/)), which are bound on application runtime to their respective Interfaces, implementing their required functions. A **BaseRepository** is present ([lcastrocv / tbnb-test / app / Repositories — Bitbucket](https://bitbucket.org/lcastrocv/tbnb-test/src/master/app/Repositories/)), which implements a base Interface and every Service extends, for commonly used functions (such as create, insert, delete, paginate, etc.)

Web routes for page viewing are configured to be handled exclusively by VueJS. Data retrieval and manipulation routes, such as Update, Store and List routes, are handled by Laravel's API routes ([lcastrocv / tbnb-test / routes / api.php — Bitbucket](https://bitbucket.org/lcastrocv/tbnb-test/src/master/routes/api.php)).

Feature testing for products are implemented, with test for different types of use cases, such as data retrieval, update, store, history creation, etc. ([lcastrocv / tbnb-test / tests / Feature / ProductTest.php — Bitbucket](https://bitbucket.org/lcastrocv/tbnb-test/src/master/tests/Feature/ProductTest.php)).

Product History insertion is handled by an **Observable**, which inserts a new record on the Product History table whenever a new **Product** is created, or updated if the quantity has changed.

## Installation

 1. Clone the project
 2. Go to the project's folder
 3. Run `composer install`
 4. Run `npm install`
 5. Copy `.env.example` to a `.env` file in the root folder, and also `.env.testing` if running PHPUNIT tests via Aristan, and configure them to their respective databases
 6. Run `php artisan key:generate`
 7. Run `php artisan migrate --seed`
 8. Add a `MIX_BASE_URL` entry to your `.env` file with the API's base url (E.g: `localhost/tbnb-test/public/api/v1/)`, which will be passed to **VueJS** to handle API requests
 9. Clear the config's cache if necessary with `php artisan config:clear`

## Quick About Me

Working 6 years in the Software Development industry, I have developed or helped develop several differents types of web-systems and APIs, that ranges from Car-Rental systems to Student Exchange systems, and much more. 

Love solving problems and learning from them. I'm always looking forward to learning new technologies, languages and anything possible that might help me grow both personally, professionally, and that enables me to help whoever I'm working to, and with, to reach their goals.

## Skills at glance

### Advanced
 - JavaScript
 - ReactJS
 - jQuery
 - HTML/CSS/SASS and Frameworks (Bootstrap and Bulma)
 - PHP
 - Laravel Framework
 - Phalcon Framework
 - RESTful APIs
 - Version control (git, svn)
 - Docker environments
- Database: MySQL
- Test-Driven Development
- Team and Project management
### Intermediate
- Vue.js
- Database: MongoDB, PostgreSQL, Redis
- UNIX/LINUX Servers
### Familiar 
-  Heroku
- React Native
- DigitalOcean Web Services
- Python
- GitLab CI/CD