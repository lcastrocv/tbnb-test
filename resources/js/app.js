/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require('./bootstrap');
import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuetify from 'vuetify/lib'
import Toasted from 'vue-toasted';

Vue.use(Vuetify)
Vue.use(VueRouter);
Vue.use(Toasted);

import vuetify from './template';
import router from './router';
import App from './App.vue';
import store from './store';
import ButtonDialog from './components/ButtonDialog';
import "./plugins/vuetify-mask.js";

console.log(process.env.MIX_BASE_URL);
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
	vuetify,
	render: h => h(App),
    el: '#app',
    router, 
    store
});
