import axios from 'axios';
import { apiBaseUrl } from '@utilities/config';

export const api = axios.create({
	baseURL: apiBaseUrl
});