import Home from '../pages/Home';
import Products from '../pages/Products';

export default [
	{
		path: '/',
		name: 'home',
		title: 'TurnoverBnB Code Challenge',
		component: Home
	},
	{
		path: '/products',
		name: 'products',
		title: 'Products',
		component: Products
	}
]