const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');

mix.webpackConfig({
	resolve: {
		alias: {
			'@utilities': path.resolve(__dirname, 'resources/js/utilities/'),
			'@services': path.resolve(__dirname, 'resources/js/services/'),
			'@components': path.resolve(__dirname, 'resources/js/components/'),
			'@pages': path.resolve(__dirname, 'resources/js/pages/')
		}
	}
})